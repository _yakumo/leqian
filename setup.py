#!/bin/env pytnon
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

setup(
    name='leqian',
    version="1.0.0",
    url='',
    license='PRIVATE',
    description='caicloud inner DC raid and disk state monitor',
    long_description=open('README.md').read(),
    author='yakumo',
    author_email='the.reason.sake@gmail.com',
    packages=find_packages(exclude=['tests']),
    zip_safe=False,
    install_requires=[
        'tornado',
        'requests',
        'beautifulsoup4',
    ],
    entry_points={
        'console_scripts': [
            'run = app.main:main',
        ],
    },
)
