#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
try:
    from BeautifulSoup import BeautifulSoup
except ImportError:
    from bs4 import BeautifulSoup

def main():
    r = requests.get('https://www.leqian.com/invest/')
    html = BeautifulSoup(r.text)
    # print html.prettify()
    tr = html.tr
    while True:
        tr = tr.find_next('tr')
        if tr is None:
            break
        tds = tr.find_all('td')
        print tds[1].find('a', target='_blank', title=True).get_text().strip()
        print tds[2].get_text().strip()
        print tds[3].get_text().strip()
        print tds[4].get_text().strip()
        print tds[6].get_text().strip()
        print '-' * 10

    r = requests.get('https://www.leqian.com/exchange/list-1-0-0-0-5-1.html')
    html = BeautifulSoup(r.text)

    tr = html.tr
    while True:
        tr = tr.find_next('tr')
        if tr is None:
            break
        tds = tr.find_all('td')
        print tds[1].find('a', target='_blank', title=True).get_text().strip()
        print tds[2].get_text().strip()
        print tds[3].get_text().strip()
        print tds[4].get_text().strip()
        print tds[6].get_text().strip()
        print '-' * 10
